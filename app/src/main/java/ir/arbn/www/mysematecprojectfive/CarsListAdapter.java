package ir.arbn.www.mysematecprojectfive;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import java.util.List;

/**
 * Created by A.R.B.N on 2/5/2018.
 */

public class CarsListAdapter extends BaseAdapter {
    Context mContext;
    List<CarModel> Cars;

    public CarsListAdapter(Context mContext, List<CarModel> Cars) {
        this.mContext = mContext;
        this.Cars = Cars;
    }

    public void addCar(CarModel Car) {
        Cars.add(Car);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return Cars.size();
    }

    @Override
    public Object getItem(int i) {
        return Cars.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View V = LayoutInflater.from(mContext).inflate(R.layout.item_list_car, null);
        TextView CarName = V.findViewById(R.id.CarName);
        TextView CarPrice = V.findViewById(R.id.CarPrice);
        ImageView CarImage = V.findViewById(R.id.CarImage);
        CarName.setText(Cars.get(i).getName());
        CarPrice.setText(Cars.get(i).getPrice() + "");

        Glide.with(mContext).load(Cars.get(i).getImage()).into(CarImage);

        return V;
    }
}

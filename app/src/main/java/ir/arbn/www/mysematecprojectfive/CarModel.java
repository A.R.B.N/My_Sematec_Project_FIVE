package ir.arbn.www.mysematecprojectfive;

/**
 * Created by A.R.B.N on 2/5/2018.
 */

public class CarModel {
    String Name;
    String Color;
    String Company;
    int Price;
    Boolean isIranian;
    String Image;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public Boolean getIranian() {
        return isIranian;
    }

    public void setIranian(Boolean iranian) {
        isIranian = iranian;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}

package ir.arbn.www.mysematecprojectfive;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {
    ListView CarsList;
    Context mContext = this;
    EditText Name, Price, URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        CarsList = findViewById(R.id.CarsList);
        Name = findViewById(R.id.Name);
        Price = findViewById(R.id.Price);
        URL = findViewById(R.id.URL);

        List<CarModel> Cars = new ArrayList<>();

        CarModel Pride = new CarModel();
        Pride.setName("Pride");
        Pride.setPrice(20000000);
        Pride.setImage("http://rasanekhodro.com/wp-content/uploads/2017/10/7f416150-8cf5-401a-a137-dc5be7fef88e.jpg");
        Cars.add(Pride);

        CarModel Logan = new CarModel();
        Logan.setName("Logan");
        Logan.setPrice(46000000);
        Logan.setImage("http://www.renault.com.eg/CountriesData/Egypt/images/Renault_Photos/Logan/files1/Colors/676_ig_w965_h543.jpg");
        Cars.add(Logan);

        CarModel Megan = new CarModel();
        Megan.setName("Megan");
        Megan.setPrice(90000000);
        Megan.setImage("https://vms.atcdn.co.uk/media/d6e342c9847a4e8d8affa94c50d6fbcc");
        Cars.add(Megan);

        CarModel Azera = new CarModel();
        Azera.setName("Azera");
        Azera.setPrice(150000000);
        Azera.setImage("http://smedia.asrekhodro.com/AsreKhodro/Uploaded/Image/1395/07/02/139507022159421141431843.jpg");
        Cars.add(Azera);

        CarModel LandCruiser = new CarModel();
        LandCruiser.setName("LandCruiser");
        LandCruiser.setPrice(560000000);
        LandCruiser.setImage("https://dmi-eddirect.edmunds-media.com/inventory/toyota/land_cruiser/2018/jtmcy7ajxj4064188/size_640x480/2018_toyota_land_cruiser_v8_1.jpg");
        Cars.add(LandCruiser);

        final CarsListAdapter adapter = new CarsListAdapter(mContext, Cars);
        CarsList.setAdapter(adapter);

        //Add New Car Into List
        findViewById(R.id.Add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String NameVal = Name.getText().toString();
                String PriceVal= Price.getText().toString();
                String URLVal= URL.getText().toString();
                CarModel Car = new CarModel();
                Car.setName(NameVal);
                Car.setPrice(Integer.parseInt(PriceVal));
                Car.setImage(URLVal);
                adapter.addCar(Car);
                Name.setText("");
                Price.setText("");
                URL.setText("");
            }
        });
    }
}
